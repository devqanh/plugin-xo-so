<?php
/*
 * Plugin Name: Xoso GET
 * Plugin URI: https://xocdiawin2888.com
 * Version: 2.0
 * Description: xocdiawin2888 , Mã shortcode : [xosomb]
 */
define('XOSO_PLUGIN_URL', plugin_dir_url(__FILE__));
define('XOSO_PLUGIN_RIR', plugin_dir_path(__FILE__));
if (!class_exists('simple_html_dom_node')) {
    require_once(XOSO_PLUGIN_RIR . 'includes/simple_html_dom.php');
}
function xoso_custom_style()
{
    wp_enqueue_style('xoso_css', XOSO_PLUGIN_URL . 'assets/css/custom.css', false, '1.0.0');
}

add_action('wp_enqueue_scripts', 'xoso_custom_style');

add_shortcode('xosomb', function () {

    $html = file_get_html("http://ketqua.net/xo-so-mien-bac");
    $data = $html->find("#outer_result_mb table", 0)->outertext;
    ob_start(); ?>

    <div class="xo-so-mb">
        <ul class="li-mb">
            <li class="mb"><a href="#" kv="mb" class="active">Miền Bắc</a></li>
            <li class="mt"><a href="#" kv="mt">Miền Trung</a></li>
            <li class="mn"><a href="#" kv="mn">Miền Nam</a></li>
        </ul>
        <div class="loading" style="display: none">
            <img src="<?php echo XOSO_PLUGIN_URL; ?>/assets/img/loading.svg"/>
        </div>
        <div class="kq-xoso">
            <?php echo $data; ?>
        </div>
    </div>
    <script>
        jQuery(document).ready(function ($) {
            $('.xo-so-mb ul a').click(function (e) {
                e.preventDefault();
                $('.xo-so-mb li a').removeClass('active');
                $(this).addClass('active');
                var kv = $(this).attr('kv');
                $('.loading').show();
                $('.kq-xoso').hide();
                $.ajax({
                    type: "post",
                    dataType: "html",
                    url: '<?php echo admin_url('admin-ajax.php');?>',
                    data: {
                        action: "xoso",
                        kv: kv
                    },
                    beforeSend: function () {

                    }
                    ,
                    success: function (response) {

                        $('.kq-xoso').html(response);
                    }
                    ,
                    error: function (jqXHR, textStatus, errorThrown) {

                        console.log('The following error occured: ' + textStatus, errorThrown);
                    }
                })
                setTimeout(function () {
                    $('.loading').hide();
                    $('.kq-xoso').show();
                }, 300);

            });
        })
    </script>
    <?php
    return ob_get_clean();
});


add_action('wp_ajax_xoso', 'xoso_function');
add_action('wp_ajax_nopriv_xoso', 'xoso_function');
function xoso_function()
{
    $kv = (isset($_POST['kv'])) ? esc_attr($_POST['kv']) : '';
    switch ($kv) :
        case 'mb' :
            $param = 'xo-so-mien-bac';
            $dom = '#outer_result_mb table';
            break;
        case 'mt' :
            $param = 'xo-so-mien-trung';
            $dom = '.kqbackground.vien table';
            break;
        case 'mn' :
            $param = 'xo-so-mien-nam';
            $dom = '.kqbackground.vien table';
            break;
        default:
            $param = 'xo-so-mien-bac';
            $dom = '#outer_result_mb table';
    endswitch;
    $html = file_get_html("http://ketqua.net/$param");
    $data = $html->find($dom, 0)->outertext;
    echo $data;
    die();
}
